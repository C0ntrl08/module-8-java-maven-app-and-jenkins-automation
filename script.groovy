def buildJar() {
    echo 'building the application...'
    sh 'mvn package'
}

def buildImage() {
    echo "building the docker image..."
    withCredentials([usernamePassword(credentialsId: 'dockerhub-c0ntrl08', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t c0ntrl08/module8-java-maven-app-for-dockerinjenkins:3.2 .'
        sh 'echo $PASS | docker login -u $USER --password-stdin'
        sh 'docker push c0ntrl08/module8-java-maven-app-for-dockerinjenkins:3.2'
    }
}

def deployApp() {
    echo 'deploying the application...'
}

return this
